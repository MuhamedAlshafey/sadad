<html>
    <body>
        <p>
            This page is for testing the hash generation method given the test data in documentation
        </p>
        <p>
            You can check <a href="docs.pdf">Smart Route Integration Guide v1.4.2.pdf</a> page 67
        </p>
        <?php
        $parameters = array(
            'Amount' => 100,
            'Channel' => '0',
            'CurrencyISOCode' => 400,
            'PaymentDescription' => urlencode("Sample Payment Description"),
            'Language' => 'En',
            'MerchantID' => 'STSPayOneM',
            'MessageID' => 1,
            'Quantity' => 1,
            'ResponseBackURL' => 'https://MerchatWebSite/PaymentResponse.do',
            'ThemeID' => 'theme1',
            'TransactionID' => '12345678901234567890',
            'Version' => '1.0',
        );
        ksort($parameters);
        $orderedString = "Y2ExNzE2NDBlZjEyNmZhZjRmMmRmY2Iy";
        foreach ($parameters as $param) {
            $orderedString .= $param;
        }
        echo "Generated Order String: " . $orderedString . '<br>';
        echo "Expected  Order String: Y2ExNzE2NDBlZjEyNmZhZjRmMmRmY2Iy1000400EnSTSPayOneM1Sample+Payment+Description1https://MerchatWebSite/PaymentResponse.dotheme1123456789012345678901.0<br>";
        $secureHash = hash('sha256', $orderedString, false);
        echo "Generated Hash: " . $secureHash . '<br>';
        echo "Expected Hash: ddf2b07b95a3d93cc1ba6cd38dc69dbc1cf36819aa0bf83f549c827f98e6a84f<br>";
        $parameters['SecureHash'] = $secureHash;
        var_dump($parameters);

        ?>

        <form action="https://srstaging.stspayone.com/SmartRoutePaymentWeb/SRPayMsgHandler" method="post" name="redirectForm">
            <?php foreach ($parameters as $key => $value) { ?>
                <input name="<?php echo $key ?>" type="hidden" value="<?php echo $value ?>"/>
            <?php } ?>
            <input type="submit" value="Proceed" />

        </form>

    </body>

</html>

