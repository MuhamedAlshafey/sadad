<?php
namespace Sadad;

/**
 * Description of HashBuilder
 *
 * @author shafey
 */
class HashBuilder
{

    protected $secretKey;

    public function __construct($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    public function build($parameters)
    {
        ksort($parameters);
        $orderedString = $this->secretKey;
        foreach ($parameters as $param) {
            $orderedString .= str_replace(' ','+',$param);
        }
        $secureHash = hash('sha256', $orderedString, false);
        return $secureHash;
    }
}
