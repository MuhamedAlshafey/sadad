<?php

namespace Sadad;

/**
 *
 * @author shafey
 */
interface ResponseParser {

    function parse($response);
}
