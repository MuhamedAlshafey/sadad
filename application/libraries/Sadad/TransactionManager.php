<?php
namespace Sadad;

/**
 * TransactionManager
 * 
 */
class TransactionManager
{

    /**
     *
     * @var HashBuilder 
     */
    protected $hashBuilder;

    /**
     *
     * @var TransactionStorageInterface 
     */
    protected $transactionStorage;

    /**
     *
     * @var TransactionRepositoryInterface 
     */
    protected $transRepo;

    /**
     * 
     * @param \Sadad\HashBuilder $hashBuilder
     * @param \Sadad\TransactionStorageInterface $transactionStorage
     */
    public function __construct(HashBuilder $hashBuilder, TransactionStorageInterface $transactionStorage, TransactionRepositoryInterface $transRepo = null)
    {
        $this->hashBuilder = $hashBuilder;
        $this->transactionStorage = $transactionStorage;
        $this->transRepo = $transRepo;
    }

    function redirectRequest($parameters)
    {
        $secureHash = $this->hashBuilder->build($parameters);
        $this->transactionStorage->store($parameters['TransactionID'], $parameters);
        $parameters['SecureHash'] = $secureHash;

        return $parameters;
    }

    function redirectRsponse($response, ResponseParser $parser)
    {
        $parsedData = $parser->parse($response);
        $storedData = $this->transactionStorage->reStore($parsedData['TransactionID']);
        $recievedHash=$parsedData['SecureHash'];
        unset($parsedData['SecureHash']);
        $secureHash = $this->hashBuilder->build($parsedData);
        if($recievedHash!==$secureHash){
            throw new Exception('Invalid Token Used!');
        }
        return $this->transRepo->save(array_merge($parsedData, $storedData));
    }

    function refund($originalTransactionID, $transactionId, $merchantID)
    {
        $data = $this->transRepo->get($originalTransactionID);

        // fill required parameters
        $parameters["MessageID"] = "4";
        $parameters["TransactionID"] = $transactionId;
        $parameters["OriginalTransactionID"] = $originalTransactionID;
        $parameters["MerchantID"] = $merchantID;
        $parameters["Amount"] = $data['Amount'];
        $parameters["CurrencyISOCode"] = $data['CurrencyISOCode'];
        $parameters["Version"] = "1.0";

        $secureHash = $this->hashBuilder->build($parameters);
        $parameters['SecureHash'] = $secureHash;

        $ch = curl_init("https://srstaging.stspayone.com/SmartRoutePaymentWeb/SRMsgHandler");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //write parameters
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));

        // Get the response
        $output = curl_exec($ch);
        curl_close($ch);
        //Output the response
        echo ($output);
        // this string is formatted as a "Query String" -
    }
}
