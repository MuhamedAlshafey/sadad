<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sadad;

/**
 * Description of RedirectResponseParser
 *
 * @author shafey
 */
class RedirectResponseParser implements ResponseParser {

    /**
     *
     * @var \Closure
     */
    protected $filterCallback;

    public function __construct($filterCallback) {
        $this->filterCallback = $filterCallback;
    }

    function parse($response) {

        $parameterNames = isset($response)?array_keys($response):[];
        $parsedResponse=[];
        foreach ($parameterNames as $paramName) {
            $exp=(explode("_", $paramName));
            $parsedResponse[array_pop($exp)] = call_user_func($this->filterCallback, $paramName);
        }
        return $parsedResponse;
    }

}
