<?php
namespace Sadad;

/**
 * TransactionGenerator
 * 
 */
class TransactionGenerator
{

    protected $salt;

    public function __construct($salt)
    {
        $this->salt = $salt;
    }

    function generate($length = 20)
    {
        $token = "";
        $codeAlphabet = md5(microtime() . $this->salt);
        $max = strlen($codeAlphabet);

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->cryptoRandSecure(0, $max - 1)];
        }

        return $token;
    }

    function cryptoRandSecure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
}
