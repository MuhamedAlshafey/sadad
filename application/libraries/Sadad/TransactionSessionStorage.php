<?php

namespace Sadad;

/**
 * Description of TransactionSessionStorage
 *
 * @author shafey
 */
class TransactionSessionStorage implements TransactionStorageInterface {

    public function __construct() {
        if (!session_id()) {
            session_start();
        }
    }

    public function reStore($transactionID) {
        if (isset($_SESSION['transactions'][$transactionID])) {
            return $_SESSION['transactions'][$transactionID];
        }
        return NULL;
    }

    public function store($transactionID, $attributes) {
        $_SESSION['transactions'][$transactionID] = $attributes;
    }

}
