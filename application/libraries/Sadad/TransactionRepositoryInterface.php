<?php
namespace Sadad;

/**
 *
 * @author shafey
 */
interface TransactionRepositoryInterface
{

    public function save($data);

    public function get($transactionId);
}
