<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AutoLoader
 *
 * @author shafey
 */
class AutoLoader {

    protected static $_registered = false;

    public function __construct() {
        if (!self::$_registered) {
            spl_autoload_register(array($this, 'autoload'));
        }
        self::$_registered = true;
    }

    public static function register() {
        if (!self::$_registered) {
            new AutoLoader();
        }
        self::$_registered = true;
    }

    function autoload($className) {
        if (class_exists($className)) {
            return TRUE;
        }

        $class = str_replace('\\', '/', $className);

        if (file_exists(APPPATH . 'libraries/' . $class . '.php')) {
            require_once (APPPATH . 'libraries/' . $class . '.php');
        }
    }

}
