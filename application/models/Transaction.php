<?php

/**
 * Transaction Model
 */
class Transaction extends CI_Model implements \Sadad\TransactionRepositoryInterface
{

    function save($data)
    {
        $newData = [
            'transaction_id' => $data['TransactionID'],
            'status_code' => $data['StatusCode'],
            'status_description' => $data['StatusDescription'],
            'currency_iso_code' => $data['CurrencyISOCode'],
            'message_id' => $data['MessageID'],
            'amount' => $data['Amount'],
            'gateway_status_code' => $data['GatewayStatusCode'],
            'gateway_status_description' => $data['GatewayStatusDescription'],
            'rrn' => $data['RRN'],
            'gateway_name' => $data['GatewayName'],
            'approval_code' => $data['ApprovalCode'],
            'item_id' => $data['ItemID'],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ];
        $this->db->insert('transaction', $newData);

        return $this->db->insert_id();
    }

    function get($transactionId)
    {
        $this->db->where('transaction_id', $transactionId);
        $query = $this->db->get('transaction');
        $data = json_decode(json_encode($query->result()), TRUE)[0];
        return $this->prepare($data);
    }

    function getList()
    {
        $query = $this->db->get('transaction');
        $results=[];
        foreach (json_decode(json_encode($query->result()), TRUE) as $row) {
            $results[]= $this->prepare($row);
        }
        return $results;
    }
    
    protected function prepare($data)
    {
        $newData = [];
        foreach ($data as $k => $value) {
            if ($k == 'rrn') {
                $newData['RRN'] = $value;
                continue;
            }
            if ($k == 'currency_iso_code') {
                $newData['CurrencyISOCode'] = $value;
                continue;
            }
            $parts = explode('_', $k);
            if (($id = array_search('id', $parts)) !== FALSE) {
                $parts[$id] = 'ID';
            }
            $newData[implode('', explode(' ', ucwords(implode(' ', $parts))))] = $value;
        }

        return $newData;
    }
}
