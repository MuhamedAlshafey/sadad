<a href="<?php echo site_url('transactions/redirect') ?>">New Redirect Message</a><br>
<table>
    <thead>
    <td>Transaction ID</td>
    <td>Status Code</td>
    <td>Status Description</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</thead>
<?php foreach ($list as $row) { ?>
<tr>
    <td><?php echo $row['TransactionID'] ?></td>
    <td><?php echo $row['StatusCode'] ?></td>
    <td><?php echo $row['StatusDescription'] ?></td>
    <td><a href="<?php echo site_url('transactions/inquiry/'.$row['TransactionID']) ?>">Inquiry</a></td>
    <td><a href="<?php echo site_url('transactions/refund/'.$row['TransactionID']) ?>">Refund</a></td>
</tr>
<?php } ?>
</table>

