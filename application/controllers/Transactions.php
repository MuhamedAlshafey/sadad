<?php

/**
 * Description of Transactions
 *
 * @author shafey
 */
class Transactions extends CI_Controller
{

    function redirect()
    {
        $mngr = new Sadad\TransactionManager(new \Sadad\HashBuilder('ZjBmMDEwNmFhYjI0MTRkZmRhY2VlMTA2'), new Sadad\TransactionSessionStorage());
        $params = $mngr->redirectRequest([
            'Amount' => 100,
            'Channel' => '0',
            'CurrencyISOCode' => 682,
            'ItemID' => 116,
            'MerchantID' => '0001000029',
            'MessageID' => 1,
            'Quantity' => 1,
            'PaymentMethod' => 1,
            'ResponseBackURL' => site_url('transactions/redirectResponse'),
            'TransactionID' => (new Sadad\TransactionGenerator('ZjBmMDEwNmFhYjI0MTRkZmRhY2VlMTA2'))->generate(),
        ]);

        $this->load->view('redirect', ['paymentParams' => $params, 'redirectURL' => 'https://srstaging.stspayone.com/SmartRoutePaymentWeb/SRPayMsgHandler']);
    }

    function redirectResponse()
    {
        $this->load->model('transaction');
        $mngr = new Sadad\TransactionManager(
            new \Sadad\HashBuilder('ZjBmMDEwNmFhYjI0MTRkZmRhY2VlMTA2')
            , new Sadad\TransactionSessionStorage()
            , $this->transaction);
        $parser = (new \Sadad\RedirectResponseParser(function($key) {
            return filter_input(INPUT_POST, $key);
        }));

        $mngr->redirectRsponse($_POST, $parser);

        var_dump($_POST);
    }

    function refund($transactionId)
    {
        $this->load->model('transaction');
//        var_dump($this->transaction->get($transactionID));exit;
        $mngr = new Sadad\TransactionManager(
            new \Sadad\HashBuilder('ZjBmMDEwNmFhYjI0MTRkZmRhY2VlMTA2')
            , new Sadad\TransactionSessionStorage()
            , $this->transaction);
        $mngr->refund($transactionId, (new Sadad\TransactionGenerator('ZjBmMDEwNmFhYjI0MTRkZmRhY2VlMTA2'))->generate(), '0001000029');
        
    }
}
