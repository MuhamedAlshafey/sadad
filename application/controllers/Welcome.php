<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{
        $this->load->model('transaction');
        
		$this->load->view('index.php',['list'=>$this->transaction->getList()]);
	}
}
